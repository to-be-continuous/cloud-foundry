#!/usr/bin/env bash
if [[ -z "$hostname" ]]
then
  echo "[ERROR] hostname env not passed"
  exit 1
fi

if [[ -z "$domain" ]]
then
  echo "[ERROR] domain env not passed"
  exit 1
fi

echo "readiness-check hook called for $environment_name on $hostname.$domain"
