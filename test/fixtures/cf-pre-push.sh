#!/usr/bin/env bash
if [[ -z "$environment_name" ]]
then
  echo "[ERROR] environment_name env not passed"
  exit 1
fi

if [[ -z "$tmp_environment_name" ]]
then
  echo "[ERROR] tmp_environment_name env not passed"
  exit 1
fi

echo "pre-push hook called for $environment_name/$tmp_environment_name"
