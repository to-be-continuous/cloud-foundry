#!/usr/bin/env bats
load "$BATS_LIBRARIES_DIR/bats-support/load.bash"
load "$BATS_LIBRARIES_DIR/bats-assert/load.bash"
load 'cf-scripts'

function setup() {
  export TTMP=$(mktemp -d)
  export CF_SCRIPTS_DIR=$TTMP
  export CI_JOB_STAGE=review
  export RUBY_RESULT=()
  export CF_MANIFEST_BASENAME="manifest"
}

function teardown() {
  echo -
}

@test "deploy without manifest should fail" {
  # WHEN
  ENV_TYPE=integration
  ENV_APP_NAME=acme1
  ENV_HOST_NAME=my-acme-host
  ENV_DOMAIN=domain.mine
  run cf_deploy
  # printf "%s\n" "${lines[@]}" > /tmp/bats.out
  # THEN
  assert_failure
}

@test "deploy with readiness check KO should fail" {
  # GIVEN
  cp test/fixtures/manifest-wo-route.yml $TTMP/manifest.yml
  cp test/fixtures/cf-readiness-check-ko.sh $TTMP/cf-readiness-check.sh
  # WHEN
  ENV_TYPE=integration
  ENV_APP_NAME=acme1
  ENV_HOST_NAME=my-acme-host
  ENV_DOMAIN=domain.mine
  run cf_deploy
  # THEN
  assert_failure
}

@test "simple deploy with single route should succeed" {
  # GIVEN
  cp test/fixtures/manifest-wo-route.yml $TTMP/manifest.yml
  cp test/fixtures/cf-readiness-check-ok.sh $TTMP/cf-readiness-check.sh
  # WHEN
  ENV_TYPE=integration
  ENV_APP_NAME=acme1
  ENV_HOST_NAME=my-acme-host
  ENV_DOMAIN=domain.mine
  run cf_deploy
  # THEN
  assert_success
  assert [ -f "$CF_SCRIPTS_DIR/generated-vars.yml" ]
  assert_line --regexp "cf push acme1 --vars-file .* --no-route"
  assert_line "cf map-route acme1 domain.mine --hostname my-acme-host"
  assert_line "readiness-check hook called for acme1 on my-acme-host.domain.mine"
  refute_line "cf start acme1"
}

@test "simple deploy with pre-push hook should succeed" {
  # GIVEN
  cp test/fixtures/manifest-wo-route.yml $TTMP/manifest.yml
  cp test/fixtures/cf-readiness-check-ok.sh $TTMP/cf-readiness-check.sh
  cp test/fixtures/cf-pre-push.sh $TTMP/
  # WHEN
  ENV_TYPE=integration
  ENV_APP_NAME=acme1
  ENV_HOST_NAME=my-acme-host
  ENV_DOMAIN=domain.mine
  run cf_deploy
  # THEN
  assert_success
  assert [ -f "$CF_SCRIPTS_DIR/generated-vars.yml" ]
  assert_line --regexp "cf push acme1 --vars-file .* --no-route"
  assert_line "cf map-route acme1 domain.mine --hostname my-acme-host"
  assert_line "readiness-check hook called for acme1 on my-acme-host.domain.mine"
  assert_line "pre-push hook called for acme1/acme1"
}

@test "simple deploy with pre-start hook should succeed" {
  # GIVEN
  cp test/fixtures/manifest-wo-route.yml $TTMP/manifest.yml
  cp test/fixtures/cf-readiness-check-ok.sh $TTMP/cf-readiness-check.sh
  cp test/fixtures/cf-pre-start.sh $TTMP/
  # WHEN
  ENV_TYPE=integration
  ENV_APP_NAME=acme1
  ENV_HOST_NAME=my-acme-host
  ENV_DOMAIN=domain.mine
  run cf_deploy
  # THEN
  assert_success
  assert [ -f "$CF_SCRIPTS_DIR/generated-vars.yml" ]
  assert_line --regexp "cf push acme1 --vars-file .* --no-route -f .* --no-start"
  assert_line "pre-start hook called for acme1/acme1"
  assert_line "cf start acme1"
  assert_line "cf map-route acme1 domain.mine --hostname my-acme-host"
  assert_line "readiness-check hook called for acme1 on my-acme-host.domain.mine"
}

@test "simple deploy with default domain should succeed" {
  # GIVEN
  cp test/fixtures/manifest-wo-route.yml $TTMP/manifest.yml
  cp test/fixtures/cf-readiness-check-ok.sh $TTMP/cf-readiness-check.sh
  # WHEN
  ENV_TYPE=integration
  ENV_APP_NAME=acme1
  ENV_HOST_NAME=my-acme-host
  run cf_deploy
  # THEN
  assert_success
  assert [ -f "$CF_SCRIPTS_DIR/generated-vars.yml" ]
  assert_line --regexp "cf push acme1 --vars-file .* --no-route"
  assert_line "cf map-route acme1 domain.intra --hostname my-acme-host"
  assert_line "readiness-check hook called for acme1 on my-acme-host.domain.intra"
}

@test "blue-green deploy with single route should succeed" {
  # GIVEN
  cp test/fixtures/manifest-wo-route.yml $TTMP/manifest.yml
  cp test/fixtures/cf-readiness-check-ok.sh $TTMP/cf-readiness-check.sh
  # WHEN
  ENV_TYPE=production
  ENV_ZERODOWNTIME=true
  ENV_APP_NAME=acme1
  ENV_HOST_NAME=my-acme-host
  ENV_DOMAIN=domain.mine
  ENV_DOMAIN_TMP=domain.intra
  run cf_deploy
  # THEN
  assert_success
  assert [ -f "$CF_SCRIPTS_DIR/generated-vars.yml" ]
  assert [ ! -f "$CF_SCRIPTS_DIR/generated-manifest.yml" ]
  assert_line --regexp "cf push acme1-tmp --vars-file .* --no-route"
  assert_line "cf map-route acme1-tmp domain.intra --hostname my-acme-host-tmp"
  assert_line "readiness-check hook called for acme1 on my-acme-host-tmp.domain.intra"
  assert_line "cf map-route acme1-tmp domain.mine --hostname my-acme-host"
  assert_line "cf delete-route domain.intra --hostname my-acme-host-tmp -f"
  assert_line "cf delete acme1 -f"
  assert_line "cf rename acme1-tmp acme1"
}

@test "simple deploy with multiple routes should succeed" {
  # GIVEN
  cp test/fixtures/manifest-w-routes.yml $TTMP/manifest.yml
  cp test/fixtures/cf-readiness-check-ok.sh $TTMP/cf-readiness-check.sh
  export RUBY_RESULT=("((hostname)).acme.com ((hostname)).secondary.intra" "$(awk  '/routes:/{no_print=1}{if(!no_print){print $0}' < test/fixtures/manifest-w-routes.yml)")
  # WHEN
  ENV_TYPE=production
  ENV_ZERODOWNTIME=false
  ENV_APP_NAME=acme1
  ENV_HOST_NAME=my-acme-host
  ENV_DOMAIN=domain.mine
  run cf_deploy
  # THEN
  assert_success
  assert [ -f "$CF_SCRIPTS_DIR/generated-vars.yml" ]
  assert_line --regexp "cf push acme1 --vars-file .*"
  refute_line --regexp "cf push acme1 --vars-file .* --hostname .*"
  refute_line --regexp "cf push acme1 --vars-file .* -d .*"
  assert_line "readiness-check hook called for acme1 on my-acme-host.domain.mine"
}

@test "blue-green deploy with multiple routes should succeed" {
  # GIVEN
  cp test/fixtures/manifest-w-routes.yml $TTMP/manifest.yml
  cp test/fixtures/cf-readiness-check-ok.sh $TTMP/cf-readiness-check.sh
  export RUBY_RESULT=("((hostname)).acme.com ((hostname)).secondary.intra" "$(awk  '/routes:/{no_print=1}{if(!no_print){print $0}' < test/fixtures/manifest-w-routes.yml)")
  # WHEN
  ENV_TYPE=production
  ENV_ZERODOWNTIME=true
  ENV_APP_NAME=acme1
  ENV_HOST_NAME=my-acme-host
  ENV_DOMAIN=domain.mine
  ENV_DOMAIN_TMP=domain.intra
  run cf_deploy
  # THEN
  assert_success
  assert [ -f "$CF_SCRIPTS_DIR/generated-vars.yml" ]
  assert [ -f "$CF_SCRIPTS_DIR/generated-manifest.yml" ]
  assert_line --regexp "cf push acme1-tmp --vars-file .* --no-route"
  assert_line "cf map-route acme1-tmp domain.intra --hostname my-acme-host-tmp"
  assert_line "readiness-check hook called for acme1 on my-acme-host-tmp.domain.intra"
  assert_line "cf map-route acme1-tmp secondary.intra --hostname my-acme-host"
  assert_line "cf map-route acme1-tmp acme.com --hostname my-acme-host"
  assert_line "cf delete-route domain.intra --hostname my-acme-host-tmp -f"
  assert_line "cf delete acme1 -f"
  assert_line "cf rename acme1-tmp acme1"
}

